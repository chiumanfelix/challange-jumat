fun main(){
    //pyramid
    var n = 5
    for (i in 0..n){
        for (j in 0..n-i){
            print(" ")
        }
        for (k in 0..i){
            print("* ")
        }
        println("")
    }
    //pyramid backward
    println()
    var m = 5
    var x = m
    var y : Int
    while (x>0){
        y=0

        while(y++ < m-x){
            print(" ")
        }
        y = 0

        while (y++ < x){
            print(" *")
        }
        println("")
        x--
    }

    //diamond
    println()

    //x pattern
    println()
    for (i in 1..n){
        for(j in 1..n){
            if(i==j || i+j==6){
                print("X")
            }
            else{
                print(" ")
            }
        }
        println()
    }

    //segitiga
    println()
    var i = 1
    var j = 1
    while (i <= 5){
        while (j <= i){
            print("*")
            j++
        }
        println()
        i++
        j = 1
    }
}