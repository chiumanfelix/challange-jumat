//Property
//Method

class Pena {
    //Property
    val tinta = "merah"
    val jari = 5
    val tinggi = 7

    //Method
    fun membukaTutupPen(){
        println("tutup pena terbuka")
    }
    fun menulis(kapasitas : Int, warna : String = "Warna Default"){ // String = .. itu nilai default
        println("pena mengeluarkan tinta sebanya $kapasitas , dan warnanya $warna")
    }
    fun menutupTutupPen(){
        println("menutup pena dengan tutup")
    }
    fun refillTinta(){
        println("melakukan refill")
    }
    fun volPena(): Double {
        return 3.14*jari*jari*tinggi
    }

    //Access Modifier -> public (terbuka terhadap semua), private (dapat diakses kelasnya sendiri),
    //protected (dapat diakses kelasnya sendiri dan turunan), internal
}