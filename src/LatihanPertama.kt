fun main(){
    var username = "ABC"
    username = "CDF"
    println(username)

    val umur:Int = 14
    println("Umur : $umur")

    val tinggiBadan = 1.5
    println("Tinggi badan : $tinggiBadan")

    val nilaiMutu: Char = 'A'
    println("Nilai Mutu : $nilaiMutu")

    val StatusLulus = true
    println("Status Lullus : $StatusLulus")

    val stringEscaped = "Nama : ABC \nAlamat \\t BSD"
    println(stringEscaped)

    val deretBilangan = intArrayOf(1, 3, 5, 7, 9)
    for (i in deretBilangan){
        println(i)
    }

    val deretBilangan2 = arrayOf(1,3,5,7)
    println("Indeks ke-2 ${deretBilangan2[2]}")

    val listName = arrayListOf("a","b")
    listName.add("c")
    println(listName)

}