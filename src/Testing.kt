//fun main(){
//    var age = readLine()!!.toInt()
//    if(age >= 17){
//        print("adult")
//    }
//    else{
//        print("under age")
//    }
//}

//fun main(){
//    var lampu = "hijau"
//    var nyala = when(lampu){
//        "hijau" -> "warna hijau"
//        "merah " -> "warna merah"
//        else -> "warna kuning"
//    }
//    println(nyala)
//}

//fun main(){
//    var num  = readLine()!!.toInt()
//    var result = when{
//        num > 0 -> "Positive"
//        num < 0 -> "Negative"
//        else -> "Zero"
//    }
//    println(result)
//}

//fun main(){
//    var sum = 0
//    var i = 1
//    while (i <= 100){
//        sum += i
//        i++
//    }
//    println(sum)
//}

//fun main(){
//    var sum =0
//    var i = 1
//    while (i<=100){
//        sum += 1
//        i++
//        if (sum==99){
//            continue
//        }
//        println(sum)
//    }
//}

//fun main(){
//    var sum = 0
//    var i = 1
//    while (i<=10){
//        i++
//        if(i%2 !=0){
//            continue
//        }
//        sum += i
//    }
//    println(sum)
//}

//fun main(){
//    var x = listOf('a', 1,'c')
//    for (i in x){
//        print(i)
//    }
//    var x = arrayOf(4,2,6,7,1) // ini dah pisah sama atas tp mager aj buat main
//    var sum = 0
//    for (num in x){
//        println("testing : $sum")
//        sum += num
//    }
//    print(sum)
//}

//fun main(){
//    for (i in 'a'..'e'){
//        println(i)
//    }
//}

//fun main(){
//    val x = 42
//    if (x in 5..10){
//        print("too big")
//    }
//    else{
//        print("small")
//    }
//}

//fun main(){
//    var iniArray = arrayOf(5,10,15,20)
//    if (20 in iniArray){
//        print("ada 20")
//    }
//}

//fun main(){
//    var r = 2..6
//    for (x in r){
//        if (10%x in r){
//            println(x)
//        }
//    }
//}

//fun main(){
//    var x = 50
//    while (true){
//        x -= 10
//        if(x<0){
//            break
//        }
//        println(x)
//    }
//}
//fun main(args: Array<String>) {
//    var hours = readLine()!!.toInt()
//    var total: Double = 0.0
//
//    if(hours<=5){
//        println(hours*1)
//    }
//    else if(hours > 5 && hours<24){
//        total = 2.5 + (0.5*hours)
//        println(total)
//    }
//    else if(hours == 24){
//        println(15)
//    }
//    else{
//        println(15+(hours-24)*0.5)
//    }
//}

//fun cal(x:Int){
//    val y = x*2
//    println(x+y)
//}
//fun main(){
//    cal(3)
//}

//fun sum(x: Int, y:Int){
//    println(x*y)
//}
//fun main(){
//    sum(5,6)
//}

fun allowed(age: Int):Boolean{
    if (age>18){
        return true
    }
    else{
        return false
    }
}
fun main(){
    val permission = allowed(18)
    println("$permission")
}

//fun cal(){
//    val f: (Int, Int) -> Int = {a,b -> a+b}
//    println(f(8,42))
//}
//fun main(){
//    cal()
//}

//fun main(){
//    var arr = arrayOf(3, 5, 7, 9)
//    var x = 0
//    arr.forEach {
//        x += it-1
//        println("sementara : $x")
//    }
//    println(x)
//}

//fun apply(x:Int, action: (Int) -> Int): Int {
//    return action(x)
//}
//
//fun main(args: Array<String>) {
//    println(apply(5, {x -> x*2}))
//    println(apply(6, {x -> x/2}))
//}

//fun f(x: String): Int {
//    var z = 0
//    for(a in x) {
//        z++
//    }
//    return z
//}
//fun main(args: Array<String>) {
//    println(f("hello"))
//}

//fun apply(x: Int): Int {
//    return x*x
//}
//fun main(args: Array<String>) {
//    val arr = arrayOf(2, 3, 4)
//    var res = 0
//    arr.forEach {
//        res += apply(it)
//        println("hasil sementara : $res")
//    }
//    println(res)
//}